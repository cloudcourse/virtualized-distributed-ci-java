package dk.lundogbendsen.vagrantjenkinsdemo;

import org.junit.Assert;

import org.junit.Test;


public class TestMeTest {

	@Test
	public void shouldSubtractIntegers() {
		Integer result = new TestMe().sub(1,2);
		
		Assert.assertEquals(new Integer(-1), result);
	}
	
	@Test
	public void shouldAddIntegers() {
		Integer result = new TestMe().add(1,2);
		
		Assert.assertEquals(new Integer(3), result);
	}
}
